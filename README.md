---
title: Esp8266 | short long story
Date: now
author: Dominique
---

# Matos
Convertisseur usb|serial pour communiquer.

## USB/Serial
* module: cp210x
Disponible [ici](https://www.aliexpress.com/item/1PCS-CP2102-USB-2-0-to-TTL-UART-Module-6Pin-Serial-Converter-STC-Replace-FT232/32716109900.html)

![usb serial](res-serial-uart.jpg)


## Esp8266
Disponible [ici](https://www.aliexpress.com/item/ESP8266-Esp-01-Remote-Serial-Port-WIFI-Wireless-Module-Through-Walls-for-Arduino-Free-Shipping/32536826125.html)

![esp8266](res-esp8266.jpg)

## Arduino


## Wire

Disponible [ici](https://www.aliexpress.com/item/Free-Shipping-Dupont-Line-120pcs-10cm-Male-to-Male-Female-to-Male-and-Female-to-Female/32829131026.html)

![wire](res-wire.jpg)

# Software
* arduino studio
* screen / miniterm
* esptool.py

# Quick start
![wire](res-reflash.png)
![wire](res-esp8266-2.jpg)


**Attention**, a bien respecter le schema.
Il faut que le cable orange soit connecté avant de brancher l'adaptateur usb pour être en mode flash.

## flash
__A sauter si vous codez en C directement.__

Trouver son firmware: list > (https://www.esp8266.com/wiki/doku.php?id=loading_firmware)
- Firmware AT:
  * https://www.electrodragon.com/w/ESP8266_AT-Command_firmware
  * permet de commander le esp8266 via des commandes de Hayes [AT](https://room-15.github.io/blog/2015/03/26/esp8266-at-command-reference/)
- Firmware NodeMCU (commander en LUA)
  * https://nodemcu.readthedocs.io/en/master/
  * https://nodemcu-build.com/ (il faut choisir les options, le site compile le firmware pour vous )
- firmware MicroPython (commander en python)

### Pour flasher facile:
```bash
esptool.py erase_flash
# unplug / replug
esptool.py write_flash -fm dout 0x0000 firmware
```

Débrancher le cable orange... et replugger.

### tester
```bash
screen /dev/ttyUSB0 115200
```

le prompt lua doit apparaitre.


## Branchement arduino
```
 Arduinot Port | Esp | Comments |
 --- | --- |
 10 | TX | virtual serial |
 11 | RX | virtual serial
 3.3V | VCC | |
 GND | GND | |
 3 | CH_PID | (need to be "1") |
 7 | GPIO_00 | . |
 - | GPIO_02 | .  |
```



# Photos
![pohto1](photo1.jpg)
![pohto2](photo2.jpg)
